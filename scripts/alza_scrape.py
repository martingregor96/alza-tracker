import csv
import datetime
import os
import pathlib
import re
import sys
from time import sleep

import firebase_admin
from bs4 import BeautifulSoup
from firebase_admin import credentials
from firebase_admin import firestore
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from get_brands import Brands
from get_products_in_stock import InStock


class Scraper:
    def __init__(self):
        self.pages = {
            "mobily": "https://www.alza.cz/mobily/18843445.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "procesory": "https://www.alza.cz/procesory/18842843.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "graficke-karty": "https://www.alza.cz/graficke-karty/18842862.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "disky-a-ssd": "https://www.alza.cz/pevne-disky/18842851.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "pameti": "https://www.alza.cz/pameti/18842853.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "zakladni-desky": "https://www.alza.cz/zakladni-desky/18842832.htm#f&cst=1&cud=0&pg=1-1000&prod=",
            "skrine-a-zdroje": "https://www.alza.cz/skrine-a-zdroje/18850751.htm#f&cst=1&cud=0&pg=1-1000&prod="
        }

    def scrape(self):
        driver = webdriver.Chrome(ChromeDriverManager().install())

        date = datetime.date.today()
        folder = date.strftime("%d.%m.%y")

        try:
            os.mkdir("csv/products/" + folder)
        except FileExistsError:
            print("Directory already exists!")

        for category, url in self.pages.items():
            driver.get(url)
            print("Loading page - " + category)

            try:
                WebDriverWait(driver, 30).until(ec.invisibility_of_element((By.CLASS_NAME, "circle-loader-container")))
                print("Page is ready!")
                sleep(2)
            except TimeoutException:
                print("Loading took too much time!")

            soup = BeautifulSoup(driver.page_source, "html.parser")

            file_name = category

            regex = re.compile(".*box browsingitem.*")
            regex_class_condition = re.compile(".*avl.*")

            with open("csv/products/" + folder + "/" + file_name + ".csv", 'w', encoding="utf-8",
                      newline='') as csv_file:
                csv_writer = csv.writer(csv_file)
                csv_writer.writerow(
                    ["productId", "image", "brand", "productName", "condition", "description", "rating", "price"])

                for product in soup.find_all("div", class_=regex):
                    product_id = product.find("span", class_="code").text
                    refined_product_id = " ".join(product_id.split())

                    img_tag = product.find("div", class_="js-block-image").img["data-src"]

                    brand = product.find("div", class_="fb").a["data-impression-brand"]

                    name_tag = product.find("div", class_="fb").a.text
                    refined_name_tag = " ".join(name_tag.split())

                    condition = product.find("div", class_=regex_class_condition).span.text
                    refined_condition = re.findall(
                        r"(Skladem|Rozbaleno skladem|Na cestě|Na objednávku|Očekáváme|"
                        r"Přijímáme předobjednávky|Použité skladem|Těšíme se|Zánovní skladem)",
                        condition[0:len(condition)])
                    refined_condition = ("-" if len(refined_condition) == 0 else refined_condition[0])

                    description = product.find("div", class_="Description").text
                    refined_description = " ".join(description.split())

                    try:
                        rating = product.find("div", class_="star-rating-wrapper")
                        if rating is not None:
                            rating = rating["title"]
                            refined_rating = rating[10:13]
                    except KeyError:
                        refined_rating = None

                    try:
                        price = product.find("span", class_="c2").text
                        refined_price = re.findall(r"\d+", price)
                        refined_price = " ".join(refined_price)
                    except AttributeError:
                        refined_price = None

                    csv_writer.writerow(
                        [refined_product_id, img_tag, brand, refined_name_tag, refined_condition, refined_description,
                         refined_rating, refined_price])
        driver.quit()
        print("Finished scraping!")
        return folder, date


class Main:
    def __init__(self):
        self.mapping = {
            u"mobily": u"smartphone",
            u"disky-a-ssd": u"drive_ssd",
            u"graficke-karty": u"gpu",
            u"pameti": u"memory",
            u"procesory": u"cpu",
            u"skrine-a-zdroje": u"case_power",
            u"zakladni-desky": u"motherboard"
        }

    def scrape_and_backup(self):
        scraper = Scraper()
        folder, date = scraper.scrape()
        self.__update_data(folder, date)

    def __update_data(self, folder_name, date):
        cred = credentials.Certificate("pricetracker.json")
        firebase_admin.initialize_app(cred)

        db = firestore.client()

        my_dir = pathlib.Path("csv/products/" + folder_name)

        for file_entry in my_dir.iterdir():
            print(file_entry)
            collection_ref = db.collection(self.mapping[file_entry.stem])
            with open(file_entry, 'r', encoding="utf-8", newline='') as csv_file:
                csv_reader = csv.reader(csv_file)
                next(csv_reader)

                row_counter = 0
                for row in csv_reader:
                    row_counter += 1

                    document_ref = collection_ref.document(row[0])
                    document = document_ref.get()

                    data = {
                        u"name": row[3],
                        u"brand": row[2],
                        u"condition": row[4],
                        u"description": row[5],
                        u"rating": float(row[6].replace(',', '.'))
                    }

                    price = int(-1 if row[7].replace(' ', '') == '' else row[7].replace(' ', ''))

                    if document.exists:
                        price_dict = document.to_dict()["price"]
                        price_dict[str(date)] = price
                    else:
                        price_dict = {str(date): price}
                    data["price"] = price_dict

                    document_ref.set(data)
                print("Updated " + str(row_counter) + " rows!")


if len(sys.argv) == 1:
    print("--- Scraping and backup ---")
    main = Main()
    main.scrape_and_backup()
elif len(sys.argv) == 3 and sys.argv[1] == "--brands":
    print("--- Get product brands ---")
    brands = Brands()
    brands.get_brands(sys.argv[2])
elif len(sys.argv) == 4 and sys.argv[1] == "--in-stock":
    print("--- Get " + sys.argv[3] + " in stock ---")
    in_stock = InStock()
    in_stock.get_products_in_stock(sys.argv[2], sys.argv[3])
else:
    print("Wrong command line arguments!")
    print("Run without arguments for scraping and backup.")
    print("Run with --brands DD.MM.YY to get CSV of brands from selected day. Selected day must be scraped beforehand.")
    print("Run with --in-stock DD.MM.YY CATEGORY to get CSV of products from selected category from selected day. "
          "Selected day for selected category must be scraped beforehand.")
