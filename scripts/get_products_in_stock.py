import csv


class InStock:
    @staticmethod
    def get_products_in_stock(date, category):
        try:
            with open("csv/products/" + date + "/" + category + ".csv", 'r', encoding="utf-8", newline='') as csv_read:
                with open("csv/" + category + "-in-stock.csv", 'w', encoding="utf-8", newline='') as csv_write:
                    csv_reader = csv.reader(csv_read)
                    csv_writer = csv.writer(csv_write)
                    first_row = True
                    for row in csv_reader:
                        if first_row:
                            first_row = False
                            csv_writer.writerow(
                                ["productId", "image", "brand", "productName", "description", "rating", "price"])
                            continue
                        csv_writer.writerow([row[0], row[1], row[2], row[3], row[5], row[6], row[7]])
        except FileNotFoundError:
            print("File csv/products/" + date + "/" + category + ".csv was not found!")
            return
