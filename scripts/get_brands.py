import csv


class Brands:
    def __init__(self):
        self.files = ["procesory", "graficke-karty", "disky-a-ssd", "pameti", "zakladni-desky", "skrine-a-zdroje"]

    def get_brands(self, date):
        brands = set()

        for file in self.files:
            try:
                with open("csv/products/" + date + "/" + file + ".csv", 'r', encoding="utf-8", newline='') as csv_file:
                    csv_reader = csv.reader(csv_file)
                    first_row = True
                    for row in csv_reader:
                        if first_row:
                            first_row = False
                            continue
                        brands.add(row[2])
            except FileNotFoundError:
                print("File csv/products/" + date + "/" + file + ".csv was not found!")
                return

        brands = list(brands)
        brands.sort()

        for brand in brands:
            print(brand)

        with open("csv/brands.csv", 'w', encoding="utf-8", newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            counter = 1
            for brand in brands:
                csv_writer.writerow([counter, brand])
                counter += 1
